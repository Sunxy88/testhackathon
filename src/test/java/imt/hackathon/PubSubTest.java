package imt.hackathon;

import imt.hackathon.pubsub.EventCenterService;
import imt.hackathon.pubsub.impl.SendMailHandler;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest
public class PubSubTest {

    @Autowired
    private EventCenterService eventCenterService;

    @Autowired
    private SendMailHandler sendMailHandler;

    @Test
    public void eventPubSubTest() {
        String event = "file_updated_send_mail";
        eventCenterService.register(event);
        eventCenterService.subscribe(event, "xi.song@imt-atlantique.net");
        Map<String, Object> message = new HashMap<>();
        message.put("publisher", "sungxisx@icloud.com");
        message.put("subject", "A file has been updated");
        message.put("content", "Test message");
        eventCenterService.publish(event, sendMailHandler, message);
    }
}
