package imt.hackathon;

import imt.hackathon.mail.model.MailDO;
import imt.hackathon.mail.service.MailService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class MailTest {

    @Autowired
    private MailService mailService;

    @Test
    public void sendMail() {
        String mailAddress1 = "xi.song@imtatlantique.net";
        String mailAddress2 = "sungxisx@icloud.com";
//        mailService.sendMail(mailAddress1, mailAddress2, "", "Test message");
        List<MailDO> mailList = mailService.getMailHistory(mailAddress2, mailAddress1);
        System.out.println(mailList);
    }

}
