package imt.hackathon.mail.dao;

import imt.hackathon.mail.model.MailDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MailDAO extends JpaRepository<MailDO, Long> {
    List<MailDO> findAllByFromAndTo(String from, String to);
}
