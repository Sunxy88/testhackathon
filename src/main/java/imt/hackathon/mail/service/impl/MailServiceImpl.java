package imt.hackathon.mail.service.impl;

import imt.hackathon.mail.dao.MailDAO;
import imt.hackathon.mail.model.MailDO;
import imt.hackathon.mail.service.MailService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public class MailServiceImpl implements MailService {

    private static final String DEFAULT_MAIL_SUBJECT = "Message From PASS";

    @Autowired
    private MailDAO mailDAO;

    @Resource
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String from;


    @Override
    public void sendMail(String from, String to, String subject, String content) {
        if (StringUtils.isEmpty(from) || StringUtils.isEmpty(to) || StringUtils.isEmpty(content)) {
            return;
        }
        if (StringUtils.isEmpty(subject)) {
            subject = DEFAULT_MAIL_SUBJECT;
        }
        MailDO mailDO = new MailDO();
        mailDO.setFrom(from);
        mailDO.setTo(to);
        mailDO.setSubject(subject);
        mailDO.setContent(content);
        mailDO.setCreatedTime(new Date());
        mailDAO.saveAndFlush(mailDO);
        sendMail(mailDO);
    }

    @Override
    public void sendMail(MailDO mailDO) {
        try {
            SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
            simpleMailMessage.setFrom(from);
            simpleMailMessage.setTo(mailDO.getTo());
            simpleMailMessage.setSubject(mailDO.getSubject());
            simpleMailMessage.setText(mailDO.getContent());
            javaMailSender.send(simpleMailMessage);
        } catch (Exception e) {
            System.err.println("Send mail exception, mailDO=" + mailDO + ", exception=" + e);
        }
    }

    @Override
    public List<MailDO> getMailHistory(String from, String to) {
        List<MailDO> sortedList = mailDAO.findAllByFromAndTo(from, to);
        List<MailDO> toFromList = mailDAO.findAllByFromAndTo(to, from);
        sortedList.addAll(toFromList);
        sortedList.sort(Comparator.comparing(MailDO::getCreatedTime));
        return sortedList;
    }
}
