package imt.hackathon.mail.service;

import imt.hackathon.mail.model.MailDO;

import java.util.List;

public interface MailService {
    /**
     * Send a mail
     * @param from
     * @param to
     * @param subject
     * @param content
     */
    void sendMail(String from, String to, String subject, String content);

    /**
     * Send a mail
     * @param mailDO
     */
    void sendMail(MailDO mailDO);

    /**
     * Get all mails between from and to, sorted by sent time
     * @param from
     * @param to
     * @return
     */
    List<MailDO> getMailHistory(String from, String to);
}
