package imt.hackathon.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import imt.hackathon.entities.Student;

public interface StudentDao extends JpaRepository<Student, Long> {

}
