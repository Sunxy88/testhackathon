package imt.hackathon.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import imt.hackathon.entities.InternshipSheet;

public interface InternshipSheetDao extends JpaRepository<InternshipSheet, Long> {

}
