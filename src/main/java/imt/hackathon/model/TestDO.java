package imt.hackathon.model;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "test")
public class TestDO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "domain")
    private String domain;
}
