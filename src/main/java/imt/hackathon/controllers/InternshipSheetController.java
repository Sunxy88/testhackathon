package imt.hackathon.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import imt.hackathon.models.InternshipSheetModel;
import imt.hackathon.services.InternshipSheetService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/hackathon")
@RequiredArgsConstructor
public class InternshipSheetController {
	
	private final InternshipSheetService internshipSheetService;
	
	@PostMapping
	@CrossOrigin(origins = "*")
	public ResponseEntity<Void> saveInternshipSheet(@RequestBody InternshipSheetModel model){
		internshipSheetService.saveInternshipSheet(model);
		return ResponseEntity.noContent().build();
		
	}

}
