package imt.hackathon.repository;

import imt.hackathon.model.TestDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TestRepository extends JpaRepository<TestDO, Integer> {
    Optional<TestDO> findById(Integer id);
}
