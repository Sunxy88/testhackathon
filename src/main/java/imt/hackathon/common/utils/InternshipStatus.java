package imt.hackathon.common.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum InternshipStatus {
	
	VALIDATED("V"),
	
	WAITING_VALIDATION("wainting for validation");
	
	private final String status;
	
	@Override
	public String toString() {
		return status;
	}
}
