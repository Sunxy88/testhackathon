package imt.hackathon.services;

import imt.hackathon.models.InternshipSheetModel;

public interface InternshipSheetService {
	
	public void saveInternshipSheet(InternshipSheetModel internshipSheet);
	
	public void deleteInternshipSheet(long id);
	
	public void validateInternshipSheet(long id);
}
