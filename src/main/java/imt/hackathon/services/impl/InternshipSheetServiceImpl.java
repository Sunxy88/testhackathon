package imt.hackathon.services.impl;

import imt.hackathon.config.PubSubConfig;
import imt.hackathon.dao.StudentDao;
import imt.hackathon.entities.Student;
import imt.hackathon.pubsub.EventCenterService;
import imt.hackathon.pubsub.impl.SendMailHandler;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import imt.hackathon.dao.InternshipSheetDao;
import imt.hackathon.dao.StudentDao;
import imt.hackathon.entities.InternshipSheet;
import imt.hackathon.entities.Student;
import imt.hackathon.mapper.InternshipSheetMapper;
import imt.hackathon.models.InternshipSheetModel;
import imt.hackathon.services.InternshipSheetService;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Service("InternshipSheetService")
@RequiredArgsConstructor
public class InternshipSheetServiceImpl implements InternshipSheetService, InitializingBean {

	private static final String NEW_INTERNSHIP_SHEET_EVENT = "new_internship_sheet_event";

	@Autowired
	private EventCenterService eventCenterService;

	@Autowired
	private SendMailHandler sendMailHandler;
	
	@Autowired
	private PubSubConfig pubSubConfig;
		
	private final InternshipSheetDao internshipSheetDao;
	
	private final StudentDao studentDao;
	
	private final InternshipSheetMapper internshipSheetMapper; 
	
	@Override
	public void saveInternshipSheet(InternshipSheetModel internshipSheet) {
		Student student = studentDao.getById(internshipSheet.getIdAuthor());
		if(student != null) {
			InternshipSheet entity = internshipSheetMapper.modelToEntity(internshipSheet, student);
			internshipSheetDao.saveAndFlush(entity);
			Map<String, Object> message = buildMessageFromEntity(entity);
			eventCenterService.publish(NEW_INTERNSHIP_SHEET_EVENT, sendMailHandler, message);
		}	
	}
	
	@Override
	public void deleteInternshipSheet(long id) {
		// TODO Auto-generated method stub
	}

	@Override
	public void validateInternshipSheet(long id) {
		// TODO Auto-generated method stub
	}

	private Map<String, Object> buildMessageFromEntity(InternshipSheet entity) {
		Student author = studentDao.getById(entity.getAuthor().getId());
		Map<String, Object> message = new HashMap<>();
		message.put("publisher", author.getEmail());
		message.put("subject", "A new internship sheet occurred");
		message.put("content", "A new internship sheet from " + author.getFirstName() + " occurred");
		return message;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		eventCenterService.register(NEW_INTERNSHIP_SHEET_EVENT);
		eventCenterService.subscribe(NEW_INTERNSHIP_SHEET_EVENT, pubSubConfig.getSubscriber());
	}
}
