package imt.hackathon.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StudentContactModel {
	
	private Long id;
	
	private String phoneContact;
	
	private String lastNameEmergencyContact;
	
	private String firstNameEmergencyContact;
	
	private String phoneEmergencyContact;

	private String relationshipType;

}

