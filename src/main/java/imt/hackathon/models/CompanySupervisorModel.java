package imt.hackathon.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CompanySupervisorModel {
	
	private Long id;
	
	private String civility;
	
	private String lastName;
	
	private String firstname;
	
	private String occupation;
	
	private String email;
	
	private String phoneSupervisor;
	
}
