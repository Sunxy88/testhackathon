package imt.hackathon.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InternshipAddressModel {
	
	private Long id;
	
	/*private String nameInternship;
		
	private String siretInternship;
	
	private String phoneInternship;
	
	private String conventionAddressInternship;
	
	private String zipCodeInternship;
	
	private String cityInternship;
	
	private String countryInternship;*/
	
	private String phoneInternship;
	
	private String internshipAddressLinkSirene;

}
