package imt.hackathon.models;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InternshipInformationsModel {
	
	private Long id; 
	
	private String title ;
	
	private String titleWithConventionLanguage ;

	private String companyDescription;

	private String missionsAndObjectives;

	private String collaboratorsNumber;

	private String whyResearchInternship;

	private String conventionLanguage;

	private Timestamp startDate ;

	private Timestamp endDate;
	
	private String workedHoursPerWeek;
	
	private String workedDaysPerWeek;
	
	private String campus;
	
	private String taf;
	
	private String ieltsLevel;
	
	private String salary;
	
	private String advantages;
	
}
