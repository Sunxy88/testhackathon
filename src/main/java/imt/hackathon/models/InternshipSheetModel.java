package imt.hackathon.models;

import java.sql.Timestamp;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InternshipSheetModel {
	
	private Long id; 
	
	private String type;
	
	private String status; 

	private Timestamp creationDate; 
	
    private Long idAuthor; 
	
	private CompanyAddressModel companyAddress;
	
	private InternshipAddressModel internshipAddress;
	
	private InternshipInformationsModel internshipInformations;
	
	private CompanySupervisorModel companySupervisor;
	
	private SignatoryInformationsModel companySignatory;
	
	private StudentContactModel studentContact;
	
	//private List<Attachment> attachments;	
	
}
