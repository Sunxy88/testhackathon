package imt.hackathon.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SignatoryInformationsModel {
	
	private Long id;
	
	private String civilitySignatory;
	
	private String lastNameSignatory;
	
	private String firstnameSignatory;
	
	private String occupationSignatory;
	
	private String emailSignatory;
	
	private String phoneSignatory;

}
