package imt.hackathon.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CompanyAddressModel {
	
	private Long id;
	
	/*private String name;
		
	private String siret;
	
	private String phone;
	
	private String conventionAddress;
	
	private String zipCode;
	
	private String city;
	
	private String country;*/
	
	private String phone;
	
	private String companyLinkSirene;

}
