package imt.hackathon.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import imt.hackathon.entities.CompanyAddress;
import imt.hackathon.entities.CompanySignatory;
import imt.hackathon.entities.CompanySupervisor;
import imt.hackathon.entities.InternshipAddress;
import imt.hackathon.entities.InternshipDescription;
import imt.hackathon.entities.InternshipSheet;
import imt.hackathon.entities.Student;
import imt.hackathon.entities.StudentContact;
import imt.hackathon.models.AttachmentModel;
import imt.hackathon.models.CompanyAddressModel;
import imt.hackathon.models.CompanySupervisorModel;
import imt.hackathon.models.InternshipAddressModel;
import imt.hackathon.models.InternshipInformationsModel;
import imt.hackathon.models.InternshipSheetModel;
import imt.hackathon.models.SignatoryInformationsModel;
import imt.hackathon.models.StudentContactModel;
import imt.hackathon.models.StudentModel;

@Component
public class InternshipSheetMapper {

	public InternshipSheetModel entityToModel(InternshipSheet value) {
		/*return new InternshipSheetModel(
				value.getId(),
				value.getType(),
				value.getStatus(),
				value.getCreationDate(),
				new StudentModel(
						value.getAuthor().getId(),
						value.getAuthor().getFirstName(),
						value.getAuthor().getLastName(),
						value.getAuthor().getEmail(),
						value.getAuthor().getTaf()	
					),
				new CompanyAddressModel(
						value.getCompanyAddress().getId(),
						value.getCompanyAddress().getName(),
						value.getCompanyAddress().getGroup(),
						value.getCompanyAddress().getSiret(),
						value.getCompanyAddress().getTelephone(),
						value.getCompanyAddress().getAdress(),
						value.getCompanyAddress().getZipCode(),
						value.getCompanyAddress().getCity(),
						value.getCompanyAddress().getCountry()
					),
				new InternshipAddressModel(
					value.getInternshipAddress().getId(),
					value.getInternshipAddress().getCompanyName(),
					value.getInternshipAddress().getSiret(),
					value.getInternshipAddress().getTelephone(),
					value.getInternshipAddress().getAdress(),
					value.getInternshipAddress().getZipCode(),
					value.getInternshipAddress().getCity(),
					value.getInternshipAddress().getCountry()
						),
				new InternshipInformationsModel(
					value.getInternshipDescription().getId(),
					value.getInternshipDescription().getTitle(),
					value.getInternshipDescription().getTitleWithConventionLanguage(),
					value.getInternshipDescription().getCompanyDescription(),
					value.getInternshipDescription().getMissionsAndObjectives(),
					value.getInternshipDescription().getCollaboratorsNumber(),
					value.getInternshipDescription().getWhyResearchInternship(),
					value.getInternshipDescription().getConventionLanguage(),
					value.getInternshipDescription().getStartDate(),
					value.getInternshipDescription().getEndDate(),
					value.getInternshipDescription().getWorkedHoursPerWeek(),
					value.getInternshipDescription().getWorkedDaysPerWeek(),
					value.getInternshipDescription().getCampus(),
					value.getInternshipDescription().getTaf(),
					value.getInternshipDescription().getIeltsLevel(),
					value.getInternshipDescription().getSalary(),
					value.getInternshipDescription().getAdvantages()
						),
				new CompanySupervisorModel(
					value.getCompanySupervisor().getId(),
					value.getCompanySupervisor().getCivility(),
					value.getCompanySupervisor().getFirstname(),
					value.getCompanySupervisor().getLastName(),
					value.getCompanySupervisor().getOccupiedPosition(),
					value.getCompanySupervisor().getEmail(),
					value.getCompanySupervisor().getPhoneNumber()
						),
				new SignatoryInformationsModel(
						value.getCompanySignatory().getId(),
						value.getCompanySignatory().getCivility(),
						value.getCompanySignatory().getFirstname(),
						value.getCompanySignatory().getLastName(),
						value.getCompanySignatory().getOccupiedPosition(),
						value.getCompanySignatory().getEmail(),
						value.getCompanySignatory().getPhoneNumber()
						),
				new StudentContactModel(
						value.getStudentConatct().getId(),
						value.getStudentConatct().getPhoneNumber(),
						value.getStudentConatct().getLastNameEmergencyContact(),
						value.getStudentConatct().getFirstNameEmergencyContact(),
						value.getStudentConatct().getPhoneEmergencyContact(),
						value.getStudentConatct().getRelationshipType()
						)
		        //value.getAttachments().ge
				);*/
		return null;
	}
	
	public InternshipSheet modelToEntity(InternshipSheetModel value , Student student) {
		
		List<InternshipSheet> listsheets = new ArrayList<>();
		return new InternshipSheet(
				value.getId(),
				value.getType(),
				value.getStatus(),
				value.getCreationDate(),
				student,
				new CompanyAddress(
						value.getCompanyAddress().getId(),
						/*value.getCompanyAddress().getName(),
						value.getCompanyAddress().getSiret(),
						value.getCompanyAddress().getPhone(),
						value.getCompanyAddress().getConventionAddress(),
						value.getCompanyAddress().getZipCode(),
						value.getCompanyAddress().getCity(),
						value.getCompanyAddress().getCountry(),*/
						value.getCompanyAddress().getPhone(),
						value.getCompanyAddress().getCompanyLinkSirene(),
						listsheets
					),
				new InternshipAddress(
					value.getInternshipAddress().getId(),
					/*value.getInternshipAddress().getNameInternship(),
					value.getInternshipAddress().getSiretInternship(),
					value.getInternshipAddress().getPhoneInternship(),
					value.getInternshipAddress().getConventionAddressInternship(),
					value.getInternshipAddress().getZipCodeInternship(),
					value.getInternshipAddress().getCityInternship(),
					value.getInternshipAddress().getCountryInternship(),*/
					value.getInternshipAddress().getPhoneInternship(),
					value.getInternshipAddress().getInternshipAddressLinkSirene(),
					listsheets
						),
				new InternshipDescription(
					value.getInternshipInformations().getId(),
					value.getInternshipInformations().getTitle(),
					value.getInternshipInformations().getTitleWithConventionLanguage(),
					value.getInternshipInformations().getCompanyDescription(),
					value.getInternshipInformations().getMissionsAndObjectives(),
					value.getInternshipInformations().getCollaboratorsNumber(),
					value.getInternshipInformations().getWhyResearchInternship(),
					value.getInternshipInformations().getConventionLanguage(),
					value.getInternshipInformations().getStartDate(),
					value.getInternshipInformations().getEndDate(),
					value.getInternshipInformations().getWorkedHoursPerWeek(),
					value.getInternshipInformations().getWorkedDaysPerWeek(),
					value.getInternshipInformations().getCampus(),
					value.getInternshipInformations().getTaf(),
					value.getInternshipInformations().getIeltsLevel(),
					value.getInternshipInformations().getSalary(),
					value.getInternshipInformations().getAdvantages()
						),
				new CompanySupervisor(
					value.getCompanySupervisor().getId(),
					value.getCompanySupervisor().getCivility(),
					value.getCompanySupervisor().getFirstname(),
					value.getCompanySupervisor().getLastName(),
					value.getCompanySupervisor().getOccupation(),
					value.getCompanySupervisor().getEmail(),
					value.getCompanySupervisor().getPhoneSupervisor(),
					listsheets
						),
				new CompanySignatory(
						value.getCompanySignatory().getId(),
						value.getCompanySignatory().getCivilitySignatory(),
						value.getCompanySignatory().getFirstnameSignatory(),
						value.getCompanySignatory().getLastNameSignatory(),
						value.getCompanySignatory().getOccupationSignatory(),
						value.getCompanySignatory().getEmailSignatory(),
						value.getCompanySignatory().getPhoneSignatory(),
						listsheets
						),
				new StudentContact(
						value.getStudentContact().getId(),
						value.getStudentContact().getPhoneContact(),
						value.getStudentContact().getLastNameEmergencyContact(),
						value.getStudentContact().getFirstNameEmergencyContact(),
						value.getStudentContact().getPhoneEmergencyContact(),
						value.getStudentContact().getRelationshipType()
						)
				);
	}

}
