package imt.hackathon.mapper;

public abstract class Mapper<T1,T2> {
	
	public abstract T2 entityToModel(T1 value);
	
	public abstract T1 modelToEntity(T2 value);

}
