package imt.hackathon.pubsub;

import java.util.Map;

public interface EventCenterService {

    /**
     * Add a new subscriber to an existed event
     * @param event
     * @param subscriber
     */
    void subscribe(String event, String subscriber);

    /**
     * register a new event
     * @param event
     */
    void register(String event);

    /**
     * publish an event that will be handled by specific handler
     * @param event
     * @param handler
     */
    void publish(String event, EventHandler handler, Map<String, Object> message);
}
