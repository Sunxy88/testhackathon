package imt.hackathon.pubsub;

import java.util.List;
import java.util.Map;

public interface EventHandler {
    void handle(List<String> subscriber, Map<String, Object> message);
}
