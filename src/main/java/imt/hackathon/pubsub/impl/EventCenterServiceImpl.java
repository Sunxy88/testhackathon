package imt.hackathon.pubsub.impl;

import imt.hackathon.pubsub.EventCenterService;
import imt.hackathon.pubsub.EventHandler;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Service
public class EventCenterServiceImpl implements EventCenterService {

    private final Map<String, List<String>> eventSubscriberMap = new ConcurrentHashMap<>(16);

    @Override
    public void subscribe(String event, String subscriber) {
        if (!eventSubscriberMap.containsKey(event)) {
            return;
        }
        List<String> subscriberList = eventSubscriberMap.get(event);
        subscriberList.add(subscriber);
        System.out.println(event + " is subscribed by " + subscriber);
    }

    @Override
    public void register(String event) {
        eventSubscriberMap.putIfAbsent(event, new CopyOnWriteArrayList<>());
    }

    @Override
    public void publish(String event, EventHandler handler, Map<String, Object> message) {
        if (!eventSubscriberMap.containsKey(event)) {
            return;
        }
        handler.handle(eventSubscriberMap.get(event), message);
    }
}
