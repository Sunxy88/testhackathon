package imt.hackathon.pubsub.impl;

import imt.hackathon.mail.service.MailService;
import imt.hackathon.pubsub.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Component
public class SendMailHandler implements EventHandler {

    @Autowired
    private MailService mailService;

    @Override
    public void handle(List<String> subscriberList, Map<String, Object> message) {
        String publisher = Objects.toString(message.get("publisher"));
        String subject = Objects.toString(message.get("subject"));
        String content = Objects.toString(message.get("content"));
        for (String subscriber : subscriberList) {
            mailService.sendMail(publisher, subscriber, subject, content);
        }
    }
}
