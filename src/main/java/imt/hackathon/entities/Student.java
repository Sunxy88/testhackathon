package imt.hackathon.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name="student")
public class Student {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="first_name")
    private String firstName;
    
	@Column(name="last_name")
    private String lastName;
    
	@Column(name="email")
    private String email;
    
	@Column(name="taf")
    private String taf; 
	
	@OneToMany(mappedBy ="author",fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<InternshipSheet> internships;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
		Student student = (Student) o;
		return id != null && Objects.equals(id, student.id);
	}

	@Override
	public int hashCode() {
		return getClass().hashCode();
	}
}
