package imt.hackathon.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="company_supervisor")
public class CompanySupervisor {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="civility")
	private String civility;
	
	@Column(name="first_name")
	private String firstname;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="occupied_position")
	private String occupiedPosition;

	@Column(name="email")
	private String email;

	@Column(name="phone_number")
	private String phoneNumber;
	
	@OneToMany(mappedBy ="companySupervisor",fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE,CascadeType.ALL})
	private List<InternshipSheet> internships;
	
}
