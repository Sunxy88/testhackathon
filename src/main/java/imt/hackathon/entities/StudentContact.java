package imt.hackathon.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="student_contact")
public class StudentContact {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="phone_number")
	private String phoneNumber;
	
	@Column(name="lastname_emergency_contact")
	private String lastNameEmergencyContact;
	
	@Column(name="firstname_emergency_contact")
	private String firstNameEmergencyContact;
	
	@Column(name="phone_number_emergency_contact")
	private String phoneEmergencyContact;

	@Column(name="relationship_type")
	private String relationshipType;

}
