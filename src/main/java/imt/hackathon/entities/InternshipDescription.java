package imt.hackathon.entities;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="internship_description")
public class InternshipDescription {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="title")
	private String title ;
	
	@Column(name="title_with_convention_language")
	private String titleWithConventionLanguage ;

	@Column(name="company_description")
	private String companyDescription;

	@Column(name="missions_and_objectives")
	private String missionsAndObjectives;

	@Column(name="collaborators_number")
	private String collaboratorsNumber;

	@Column(name="why_research_internship")
	private String whyResearchInternship;

	@Column(name="convention_language")
	private String conventionLanguage;

	@Column(name="start_date")
	private Timestamp startDate ;

	@Column(name="end_date")
	private Timestamp endDate;
	
	@Column(name="worked_hours_per_week")
	private String workedHoursPerWeek;
	
	@Column(name="worked_days_per_week")
	private String workedDaysPerWeek;
	
	@Column(name="campus")
	private String campus;
	
	@Column(name="taf")
	private String taf;
	
	@Column(name="ielts_level")
	private String ieltsLevel;
	
	@Column(name="salary")
	private String salary;
	
	@Column(name="advantages")
	private String advantages;

}
