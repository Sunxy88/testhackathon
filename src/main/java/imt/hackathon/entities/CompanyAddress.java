package imt.hackathon.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="company_address")
public class CompanyAddress {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/*@Column(name="name")
	private String name;
		
	@Column(name="siert")
	private String siret;
	
	@Column(name="telephone")
	private String telephone;
	
	@Column(name="adress")
	private String adress; 
	
	@Column(name="zip_code")
	private String zipCode;
	
	@Column(name="city")
	private String city; 
	
	@Column(name="country")
	private String country;*/
	
	@Column(name="telephone")
	private String telephone;
	
	@Column(name="company_link_sirene")
	private String companyLinkSirene;
	
	@OneToMany(mappedBy ="companyAddress",fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE,CascadeType.ALL})
	private List<InternshipSheet> internships;
}
