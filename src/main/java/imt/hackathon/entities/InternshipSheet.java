package imt.hackathon.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name="internship_sheet")
public class InternshipSheet {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="type")
	private String type;

	@Column(name="status")
	private String status;

	@Column(name="creation_date")
	private Timestamp creationDate; 

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="id_student",nullable=false)
	private Student author;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_company_address",nullable=false)
	private CompanyAddress companyAddress;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_internship_address",nullable=false)
	private InternshipAddress internshipAddress;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_internship_description",nullable=false)
	private InternshipDescription internshipDescription;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_company_supervisor",nullable=false)
	private CompanySupervisor companySupervisor;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_company_signatory",nullable=false)
	private CompanySignatory companySignatory;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_student_contact",nullable=false)
	private StudentContact studentConatct;
	
	/*@JoinColumn(name="id_student")
	private EstimatedBudget estimatedBudget;
	
	@JoinColumn(name="id_student")
	private EmergencyContact emergencyContact;*/
	
	/*@OneToMany(cascade = {CascadeType.REMOVE, CascadeType.REFRESH , CascadeType.MERGE})
	@JoinColumn(name="id_attachments")
	private List<Attachment> attachments;*/

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
		InternshipSheet that = (InternshipSheet) o;
		return id != null && Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {
		return getClass().hashCode();
	}
}
